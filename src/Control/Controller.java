package Control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JTextField;

import View.View;
import Model.*;

public class Controller {

	private View view;
	private static int row;
	private static int accRow;
	private static int nrPersons;
	private Bank bank;// = new Bank();
	
	public Controller(View view) {
		
		this.view = view;
		bank = getSerializableFile();
		fill();
		control();
	}
	
	public void control() {
		view.createPersonBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person person;
				int id = Integer.parseInt(view.persIdField.getText());
				String firstName = view.firstNameField.getText();
				String lastName = view.lastNameField.getText();
				String address = view.persAddrField.getText();
				String email = view.emailField.getText();
				String phoneNumber = view.phoneField.getText();
				person = new Person(id, firstName, lastName, address, email, phoneNumber);
				bank.addPerson(person);
				nrPersons++;
				view.personsTableModel.addRow(new Object[] {id, firstName, lastName, address, email, phoneNumber });
			}
		});
		
		view.deletePersonBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person person;
				int id = Integer.parseInt(view.persIdField.getText());
				String firstName = view.firstNameField.getText();
				String lastName = view.lastNameField.getText();
				String address = view.persAddrField.getText();
				String email = view.emailField.getText();
				String phoneNumber = view.phoneField.getText();
				person = new Person(id, firstName, lastName, address, email, phoneNumber);
				bank.removePerson(person);
				nrPersons--;
				view.personsTableModel.removeRow(row);
			}
		});
		
		view.updatePersonBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person person = bank.clientsTable.get(row+1);
				Person newPerson;
				int id = Integer.parseInt(view.persIdField.getText());
				String firstName = view.firstNameField.getText();
				String lastName = view.lastNameField.getText();
				String address = view.persAddrField.getText();
				String email = view.emailField.getText();
				String phoneNumber = view.phoneField.getText();
				newPerson = new Person(id, firstName, lastName, address, email, phoneNumber);
				bank.updatePerson(person, newPerson);
				int removeId = person.getId() - 1;
				view.personsTableModel.removeRow(removeId);
				view.personsTableModel.insertRow(row, new Object[] {id, firstName, lastName, address, email, phoneNumber });
			}
		});
		
		view.personsTable.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
	        	row = view.personsTable.rowAtPoint(e.getPoint());
	        	System.out.println("You clicked at row " + row);
	        }
	    });
		
		view.createSavingAccountBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int rand = row + 1;
				Person person = bank.clientsTable.get(rand);
				Account account;
				int personId = person.getId();
				int id = Integer.parseInt(view.accIdField.getText());
				int PIN = Integer.parseInt(view.PINField.getText());
				String accType = view.accTypeField.getText();
				double balance = Double.parseDouble(view.balanceField.getText());
				double interest = Double.parseDouble(view.interestField.getText());
				account = new SavingAccount(id, PIN, accType, balance, interest);
				bank.addAccount(person, account);
				view.accountsTableModel.addRow(new Object[] { personId, id, PIN, accType, balance, interest });
			}
		});
		
		view.createSpendingAccountBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int rand = row + 1;
				Person person = bank.clientsTable.get(rand);
				Account account;
				int personId = person.getId();
				int id = Integer.parseInt(view.accIdField.getText());
				int PIN = Integer.parseInt(view.PINField.getText());
				String accType = view.accTypeField.getText();
				double balance = Double.parseDouble(view.balanceField.getText());
				account = new SpendingAccount(id, PIN, accType, balance);
				bank.addAccount(person, account);
				view.accountsTableModel.addRow(new Object[] { personId, id, PIN, accType, balance });
			}
		});
		
		view.deleteAccountBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Account account;
				Person person = bank.clientsTable.get(row + 1);
				int id = Integer.parseInt(view.accIdField.getText());
				int PIN = Integer.parseInt(view.PINField.getText());
				String accType = view.accTypeField.getText();
				double balance = Double.parseDouble(view.balanceField.getText());
				double interest=0;
				if (accType.equals("Saving account")) {
					interest = Double.parseDouble(view.interestField.getText());
				}
				if (accType.equals("Saving account")) {
					account = new SavingAccount(id, PIN, accType, balance, interest);
					bank.removeAccount(person, account);
				}
				if (accType.equals("Spending account")) {
					account = new SpendingAccount(id, PIN, accType, balance);
					bank.removeAccount(person, account);
				}
				view.accountsTableModel.removeRow(accRow);
			}
		});
		
		view.accountsTable.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent ev) {
	        	accRow = view.accountsTable.rowAtPoint(ev.getPoint());
	        	System.out.println("You clicked at row " + accRow);
	        }
	    });
		
		view.depositBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person person = bank.clientsTable.get(row + 1);
				int id = Integer.parseInt(view.accIdField.getText());
				double amount = Double.parseDouble(view.amountField.getText());
				for(Person p : bank.clientsTable.values()) {
					if (p.getId() == person.getId()) {
						for (Account acc1 : p.savingAccounts) {
							if (acc1.getId() == id) {
								acc1.addObserver(p);
								acc1.deposit(amount);
								view.accountsTableModel.setValueAt(acc1.getBalance(), accRow, 4);
								break;
							}
						}
						for (Account acc1 : p.spendingAccounts) {
							if (acc1.getId() == id) {
								acc1.addObserver(p);
								acc1.deposit(amount);
								view.accountsTableModel.setValueAt(acc1.getBalance(), accRow, 4);
								break;
							}
						}
						break;
					}
				}
			}
		});
		
		view.withdrawBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person person = bank.clientsTable.get(row + 1);
				int id = Integer.parseInt(view.accIdField.getText());
				double amount = Double.parseDouble(view.amountField.getText());
				for(Person p : bank.clientsTable.values()) {
					if (p.getId() == person.getId()) {
						for (Account acc1 : p.savingAccounts) {
							if (acc1.getId() == id) {
								acc1.addObserver(p);
								acc1.withdraw(amount);
								view.accountsTableModel.setValueAt(acc1.getBalance(), accRow, 4);
								break;
							}
						}
						for (Account acc1 : p.spendingAccounts) {
							if (acc1.getId() == id) {
								acc1.addObserver(p);
								acc1.withdraw(amount);
								view.accountsTableModel.setValueAt(acc1.getBalance(), accRow, 4);
								break;
							}
						}
						break;
					}
				}
			}
		});
		
		view.clearBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(JTextField textField : view.jTextFields)
					textField.setText(null);
			}
		});
		
		view.exitBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setSerializable();
				System.exit(0);
			}
		});
	}
	
	private Bank getSerializableFile() {
		Bank bank = null;
		try {
			FileInputStream serializableFile = new FileInputStream("serializationFile.ser");
            ObjectInputStream objSerializableInFile = new ObjectInputStream(serializableFile);
            bank = (Bank) objSerializableInFile.readObject();
            objSerializableInFile.close();
            serializableFile.close();
		}catch(Exception e) {
			System.out.println(e);
		}
		return bank;
	}
	
	private void setSerializable(){
        try
        {
            FileOutputStream serializableFile = new FileOutputStream("serializationFile.ser");
            ObjectOutputStream objSerializableOutFile = new ObjectOutputStream(serializableFile);
            objSerializableOutFile.writeObject(bank);
            objSerializableOutFile.close();
            serializableFile.close();
        } catch (Exception e)
        {
            System.out.println(e);
        }
    }
	
	private void fill() {
		for (Person pers : bank.clientsTable.values()) {
			int id = pers.getId();
			String firstName = pers.getFirstName();
			String lastName = pers.getLastName();
			String address = pers.getAddress();
			String email = pers.getEmail();
			String phoneNumber = pers.getPhoneNumber();
			view.personsTableModel.addRow(new Object[] {id, firstName, lastName, address, email, phoneNumber });
			
			for (Account acc : pers.savingAccounts) {
				int accId = acc.getId();
				int PIN = acc.getPin();
				String accType = acc.getAccountType();
				double balance = acc.getBalance();
				double interest = ((SavingAccount) acc).getMonthlyInterest();
				view.accountsTableModel.addRow(new Object[] { id, accId, PIN, accType, balance, interest });
			}
			
			for (Account acc : pers.spendingAccounts) {
				int accId = acc.getId();
				int PIN = acc.getPin();
				String accType = acc.getAccountType();
				double balance = acc.getBalance();
				view.accountsTableModel.addRow(new Object[] { id, accId, PIN, accType, balance });
			}
		}
	}
}
