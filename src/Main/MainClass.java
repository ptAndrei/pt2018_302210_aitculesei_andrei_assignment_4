package Main;
import Model.*;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import View.View;
import Control.Controller;

public class MainClass {

	public static void main(String[] args) {
		/*
		try {
			
			FileOutputStream serializableFile = new FileOutputStream("serializationFile.ser");
			ObjectOutputStream objSerializationFile = new ObjectOutputStream(serializableFile);
			
			Hashtable<Integer, Person> table = new Hashtable<Integer, Person>();
			Person person = new Person(1, "Andrei", "Aitculesei", "Calea Turzii", "andrei.aitculesei@yahoo.com", "0744284365");
			Account acc1 = new SavingAccount(1120, 1298, "Saving account", 2000, 0.25);
			Account acc2 = new SpendingAccount(2011, 2189, "Spending account", 10000);
			Account acc3 = new SpendingAccount(1000, 2314, "Spending account", 99239);
			person.createSavingAccount(acc1);
			person.createSpendingAccount(acc2);
			person.createSpendingAccount(acc3);
			
			table.put(person.hashCode(), person);
			Bank bank = new Bank();
			
			objSerializationFile.writeObject(bank);
			objSerializationFile.close();
			serializableFile.close();
			
		}catch(Exception e) {
			
			System.out.println(e.toString());
		}
		*/
		View v = new View();
		Controller c = new Controller(v);
	}
}
