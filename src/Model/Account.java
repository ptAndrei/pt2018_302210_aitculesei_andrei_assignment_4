package Model;

import java.io.Serializable;
import java.util.InputMismatchException;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable {

	private int id;
	private int PIN;
	private String accountType;
	private double balance; //sold
	private java.util.Date dateCreated;
	
	public Account() {
		
		balance = 0;
		dateCreated = new java.util.Date();
	}
	
	public Account(int newId, int pin, double newBalance) {
		
		id = newId;
		PIN = pin;
		balance = newBalance;
		dateCreated = new java.util.Date();
	}
	
	//Set
	public void setId(int id) {
		
		try {
			
			this.id = id;
		}catch(InputMismatchException e) {
			
			System.out.println("Invalid format of id!");
		}
	}
	
	public void setPin(int PIN) {
		
		if(PIN >= 1000 && PIN <= 9999)
			this.PIN = PIN;
		else
			System.out.println("Invalid PIN number!");
	}
	
	public void setAccountType(String accountType) {
		
		if (accountType.equals("Saving account") || accountType.equals("Spending account"))
			this.accountType = accountType;
	}
	
	public void setBalance(double balance) {
		
		try {
			
			this.balance = balance;
		}catch(InputMismatchException e) {
			
			System.out.println("Invalid format of balance!");
		}
	}
	
	//Get
	public int getId() {
		
		return id;
	}
	
	public int getPin() {
		
		return PIN;
	}
	
	public String getAccountType() {
		
		return accountType;
	}
	
	public double getBalance() {
		
		return balance;
	}
	
	public java.util.Date getCreatedDate(){
		
		return dateCreated;
	}
	
	public abstract void withdraw(double amount);
	
	public void deposit(double amount) {
		
		balance += amount;
		String notificare = "Deposited with succes!";
		setChanged();
		notifyObservers(notificare);
	}
	
	@Override
	public int hashCode() {
		
		return id;
	}
	
	@Override
    public boolean equals(Object object)
    {
        Account account = (Account)object;
        if(account.getId() == this.getId())
        {
            return true;
        }
        return false;
    }
}
