package Model;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Set;

public class Bank implements BankProc, Serializable{

	public Hashtable<Integer, Person> clientsTable;
	
	public Bank() {

		clientsTable = new Hashtable<Integer, Person>();
	}
	
	public Bank(Hashtable<Integer, Person> table) {
		
		table = new Hashtable<Integer, Person>();
	}

	public void addPerson(Person person) {

		assert clientsTable.contains(person) == false : "Person found!";
		
		assert clientsTable.containsKey(person.hashCode()) == false : "This person already exist!";
		if (clientsTable.contains(person) == false) {
			
			int size = clientsTable.size();
			clientsTable.put(person.hashCode(), person);
			assert size == (clientsTable.size() - 1) : "Could not add a new person!";
		}
	}
	
	public void removePerson(Person person) {
		
		assert clientsTable.contains(person) == true : "Person not found!";
		
		assert clientsTable.containsKey(person.hashCode()) == true : "This person does not exist!";
		int size = clientsTable.size();
		clientsTable.remove(person.hashCode());
		assert size == (clientsTable.size() + 1) : "Could not delete that person!";
	}
	
	public void updatePerson(Person person, Person newPerson) {
		
		Set<Integer> ints = clientsTable.keySet();
		assert ints.contains(person.hashCode()) == true : "Person does not exist!";
		
		boolean updated = false;
		for (Integer integer : ints) {
			
			if (integer == person.hashCode()) {
				
				person.setId(newPerson.getId());
				person.setFirstName(newPerson.getFirstName());
				person.setLastName(newPerson.getLastName());
				person.setPersonAddress(newPerson.getAddress());
				person.setEmail(newPerson.getEmail());
				person.setPhoneNumber(newPerson.getPhoneNumber());
				updated = true;
			}
		}
		assert updated == true : "The person was not updated!";
	}
	
	public void addAccount(Person person, Account account) {
		
		assert clientsTable.contains(person) == true : "Person not found!";
		
		
		if (clientsTable.contains(person) == true) {
			
			if (account.getAccountType().equals("Saving account")) {
				
				assert person.savingAccounts.contains(account) == false : "Account already exist!";
				int size = person.savingAccounts.size();
				person.createSavingAccount(account);
				assert size == (person.savingAccounts.size() - 1) : "Could not add a new account!";
			}
			if (account.getAccountType().equals("Spending account")) {
				
				assert person.spendingAccounts.contains(account) == false : "Account already exist!";
				int size = person.spendingAccounts.size();
				person.createSpendingAccount(account);
				assert size == (person.spendingAccounts.size() - 1) : "Could not add a new account!";
			}
		}
	}
	
	public void removeAccount(Person person, Account account) {
		
		assert clientsTable.contains(person) == true : "Person not found!";
		boolean found = true;
		for(Person p : clientsTable.values()) {
			if (p.getId() == person.getId()) {
				
				for(Account acc : p.savingAccounts) {
					
					if (acc.hashCode() == account.hashCode()) {
						
						int size = p.savingAccounts.size();
						p.savingAccounts.remove(acc);
						assert size == (p.savingAccounts.size() + 1) : "Could not delete that account!";
						found = false;
						break;
					}
				}
				for(Account acc : p.spendingAccounts) {
					
					if (acc.hashCode() == account.hashCode()) {
						
						int size = p.spendingAccounts.size();
						p.spendingAccounts.remove(acc);
						assert size == (p.spendingAccounts.size() + 1) : "Could not delete that account!";
						found = false;
						break;
					}
				}
				break;
			}
		}
		assert found == false : "Account not found!";
	}
	
	public void updateAccount(Person person, Account account, int newId, int newPIN) {
		
		assert clientsTable.contains(person) == true : "Person not found!";
		if (account.getAccountType().equals("Saving account"))
			assert person.savingAccounts.contains(account) == true : "Account not found!";
		if (account.getAccountType().equals("Spending account"))
			assert person.spendingAccounts.contains(account) == true : "Account not found!";
		
		if (clientsTable.contains(person) == true && person.savingAccounts.contains(account) == true) {
		
			if (newId != 0)
				account.setId(newId);
			if (newPIN != 0)
				account.setPin(newPIN);
		}
		if (clientsTable.contains(person) == true && person.spendingAccounts.contains(account) == true) {
			
			if (newId != 0)
				account.setId(newId);
			if (newPIN != 0)
				account.setPin(newPIN);
		}
	}
	/*
	public void bank(Account account) {

		int PIN = 0;
		int option;
		double amount;
		boolean exit = false;
		Scanner input = new Scanner(System.in);

		System.out.println("Enter your 4 digit PIN number:");
		try {

			PIN = input.nextInt();
		}catch(InputMismatchException e) {

			System.out.print("Incorrect PIN number!"+e);
		}

		if (PIN != account.getPin()) {

			System.out.println("Introduced PIN it's incorrect!");
			System.exit(0);
		}

		do {

			System.out.println("Select one of the following options:");
			System.out.println(" 1. Print balance");
			System.out.println(" 2. Deposit");
			System.out.println(" 3. Withdraw");
			System.out.println(" 4. Exit");
			System.out.println("Type your option number here:");
			option = input.nextInt();

			switch(option) {

			case 1: System.out.println(account.getBalance());
			break;

			case 2: System.out.println("Enter your cash amount to be deposited:");
			amount = input.nextInt();
			account.deposit(amount);
			System.out.println("You deposited with succes "+amount);
			break;

			case 3: System.out.println("Enter your cash amount to be withdrawed:");
			amount = input.nextInt();
			account.withdraw(amount);
			System.out.println("You withdrawed with succes "+amount);
			break;

			case 4: exit = true;
			break;
			
			default: System.out.println("Wrong option!");
			break;
			}
		}while(exit);
	}*/
}
