package Model;

public interface BankProc {

	/*
	 * @precond
	 * 		bank.contains(person) == false
	 * @postcond
	 * 		bank.nrPersons() == bank.nrPersons() + 1
	 */
	public void addPerson(Person person);
	
	/*
	 * @precond
	 * 		bank.contains(person) == true
	 * @postcond
	 * 		bank.nrPersons() == bank.nrPersons() - 1
	 */
	public void removePerson(Person person);
	
	
	public void updatePerson(Person person, Person newPerson);
	
	/*
	 * @precond
	 * 		bank.contains(person) == true
	 * @postcond
	 * 		bank.nrAccounts() == bank.nrAccounts() + 1
	 */
	public void addAccount(Person person, Account account);
	
	/*
	 * @precond
	 * 		bank.contains(person) == true
	 * @postcond
	 * 		bank.nrAccounts() == bank.nrAccounts() - 1
	 */		
	public void removeAccount(Person person, Account account);
	
	public void updateAccount(Person person, Account account, int newId, int newPIN);
}
