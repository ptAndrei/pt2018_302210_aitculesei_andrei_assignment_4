package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Person implements Observer, Serializable{

	private int id;
	private String firstName;
	private String lastName;
	private String personAddress;
	private String email;
	private String phoneNumber;
	private String dateUpdate;
	public ArrayList<Account> savingAccounts = new ArrayList<Account>();
	public ArrayList<Account> spendingAccounts = new ArrayList<Account>();
	
	public Person(int id, String firstName, String lastName, String personAddress, String email, String phoneNumber) {
		
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.personAddress = personAddress;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}
	
	public void createSavingAccount(Account account) {
		
		savingAccounts.add(account);
	}
	
	public void createSpendingAccount(Account account) {
		
		spendingAccounts.add(account);
	}
	
	public void setId(int id) {
		
		this.id = id;
	}
	
	public void setFirstName(String firstName) {
		
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		
		this.lastName = lastName;
	}
	
	public void setPersonAddress(String personAddress) {
		
		this.personAddress = personAddress;
	}
	
	public void setEmail(String email) {
		
		this.email = email;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		
		this.phoneNumber = phoneNumber;
	}
	
	public int getId() {
		
		return id;
	}
	
	public String getFirstName() {
		
		return firstName;
	}
	
	public String getLastName() {
		
		return lastName;
	}
	
	public String getAddress() {
		
		return personAddress;
	}
	
	public String getEmail() {
		
		return email;
	}
	
	public String getPhoneNumber() {
		
		return phoneNumber;
	}
	
	public String getSavingAccounts() {
		
		return savingAccounts.toString();
	}
	
	public String getSpendingAccounts() {
		
		return spendingAccounts.toString();
	}
	
	@Override
	public int hashCode() {
		
		return id;
	}
	
	@Override
	public boolean equals(Object object) {
		
		Person person = (Person)object;
		if (person.getId() == this.getId())
			return true;
		return false;
	}
	
	public String toString() {
		
		return "Person "+firstName+" "+lastName+", with id="+id+", address="+personAddress+", email="+email+", phone="+phoneNumber;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
		dateUpdate = (String)arg1;
		System.out.println(dateUpdate);
	}
}
