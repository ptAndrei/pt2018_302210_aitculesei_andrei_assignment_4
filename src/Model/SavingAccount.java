package Model;

public class SavingAccount extends Account {

	private static double monthlyInterest; //dobanda
	
	public SavingAccount() {
		
		super();
	}
	
	public SavingAccount(int id, int PIN, String accType, double balance, double interest) {
		
		super(id, PIN, balance);
		monthlyInterest = interest;
		this.setAccountType(accType);
	}
	
	public double getInterest() {
		
		double newBalance = this.getBalance() * monthlyInterest;
		this.setBalance(this.getBalance()+newBalance);
		return this.getBalance();
	}
	
	public static void setMonthlyInterest(double newMonthlyInterest) {
		
		monthlyInterest = newMonthlyInterest;
	}
	
	public static double getMonthlyInterest() {
		
		return monthlyInterest;
	}
	
	public void withdraw(double amount) {
		
		String notificare = "Withdraw with succes!";
		setChanged();
		notifyObservers(notificare);
		this.setBalance(0);
	}
	
	public String toString() {
		
		return "Account of type "+this.getAccountType()+" with id="+this.getId()+", PIN="+this.getPin()+" and balance="+this.getBalance()+" was created in "+this.getCreatedDate();
	}
}
