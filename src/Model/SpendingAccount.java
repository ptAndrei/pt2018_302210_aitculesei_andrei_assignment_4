package Model;

public class SpendingAccount extends Account {

	public SpendingAccount() {
		
		super();
	}
	
	public SpendingAccount(int id, int PIN, String accType, double balance) {
		
		super(id, PIN, balance);
		this.setAccountType(accType);
	}
	
	public void withdraw(double amount) {
		
		String notificare;
		if (amount <= this.getBalance()) {
			
			notificare = "Withdraw with succes!";
			setChanged();
			notifyObservers(notificare);
			this.setBalance(this.getBalance() - amount);//balance -= amount;
		}
		else
			System.out.println("Fond insuficient!");
	}
	
	public String toString() {
		
		return "Account of type "+this.getAccountType()+" with id="+this.getId()+", PIN="+this.getPin()+" and balance="+this.getBalance()+" was created in "+this.getCreatedDate();
	}
}
