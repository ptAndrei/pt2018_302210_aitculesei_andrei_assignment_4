package View;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.List;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class View {

	public DefaultTableModel personsTableModel = new DefaultTableModel();
	public DefaultTableModel accountsTableModel = new DefaultTableModel();
	
	String[] personColumnNames = {"id", "first name", "last name", "address", "email", "phone number"};
	public JTable personsTable;
	
	String[] accountColumnNames = {"ip person", "id","PIN","Account type", "balance", "interest"};
	public JTable accountsTable;
	
	private final JFrame mainFrame = new JFrame("Bank");
	private JPanel tablesPanel = new JPanel();
	private JPanel tablePersonsPanel = new JPanel();
	private JPanel tableAccountsPanel = new JPanel();
	private JPanel optionsPanel = new JPanel();
	private JPanel personOptionsPanel = new JPanel();
	private JPanel accountOptionsPanel = new JPanel();
	private JPanel operationOptionsPanel = new JPanel();
	
	public ArrayList<JTextField> jTextFields = new ArrayList<JTextField>();
	
	private JLabel persIdLabel = new JLabel("personId:");
	public JTextField persIdField = new JTextField();
	private JLabel firstNameLabel = new JLabel("First name:");
	public JTextField firstNameField = new JTextField();
	private JLabel lastNameLabel = new JLabel("Last name:");
	public JTextField lastNameField = new JTextField();
	private JLabel persAddrLabel = new JLabel("Person address:");
	public JTextField persAddrField = new JTextField();
	private JLabel emailLabel = new JLabel("Email:");
	public JTextField emailField = new JTextField();
	private JLabel phoneLabel = new JLabel("Phone number:");
	public JTextField phoneField = new JTextField();
	private JLabel accIdLabel = new JLabel("accountId:");
	public JTextField accIdField = new JTextField();
	private JLabel PINLabel = new JLabel("PIN:");
	public JTextField PINField = new JTextField();
	private JLabel accTypeLabel = new JLabel("Account type:");
	public JTextField accTypeField = new JTextField();
	private JLabel balanceLabel = new JLabel("Balance:");
	public JTextField balanceField = new JTextField();
	private JLabel interestLabel = new JLabel("Interest:");
	public JTextField interestField = new JTextField();
	private JLabel amountLabel = new JLabel("Amount:");
	public JTextField amountField = new JTextField();
	
	public JButton createPersonBtn = new JButton("Create person");
	public JButton deletePersonBtn = new JButton("Delete person");
	public JButton updatePersonBtn = new JButton("Update person");
	public JButton createSavingAccountBtn = new JButton("Create saving account");
	public JButton createSpendingAccountBtn = new JButton("Create spending account");
	public JButton deleteAccountBtn = new JButton("Delete account");
	public JButton updateAccountBtn = new JButton("Update account");
	public JButton withdrawBtn = new JButton("Withdraw");
	public JButton depositBtn = new JButton("Deposit");
	public JButton clearBtn = new JButton("Clear");
	public JButton exitBtn = new JButton("Exit");
	
	public View() {
		
		addTextFields();
		view();
	}
	
	public void addTextFields() {
		
		jTextFields.add(PINField);
		jTextFields.add(accIdField);
		jTextFields.add(accTypeField);
		jTextFields.add(amountField);
		jTextFields.add(balanceField);
		jTextFields.add(emailField);
		jTextFields.add(firstNameField);
		jTextFields.add(lastNameField);
		jTextFields.add(interestField);
		jTextFields.add(persAddrField);
		jTextFields.add(persIdField);
		jTextFields.add(phoneField);
	}
	
	public void view() {
		
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.setSize(new Dimension(1000, 1000));
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setLayout(new GridLayout(2, 1));
		mainFrame.add(tablesPanel);
		mainFrame.add(optionsPanel);
		
		personsTable = new JTable(personsTableModel);
		for (String personColumnName : personColumnNames)
			personsTableModel.addColumn(personColumnName);
		tablePersonsPanel.setLayout(new BorderLayout());
		tablePersonsPanel.add(personsTable, BorderLayout.CENTER);
		tablePersonsPanel.add(new JScrollPane(personsTable));
		
		accountsTable = new JTable(accountsTableModel);
		for (String accountColumnName : accountColumnNames)
			accountsTableModel.addColumn(accountColumnName);
		tableAccountsPanel.setLayout(new BorderLayout());
		tableAccountsPanel.add(accountsTable, BorderLayout.CENTER);
		tableAccountsPanel.add(new JScrollPane(accountsTable));
		
		tablesPanel.setLayout(new GridLayout(1, 2));
		optionsPanel.setLayout(new GridLayout(1, 3));
		
		tablesPanel.add(tablePersonsPanel);
		tablesPanel.add(tableAccountsPanel);
		
		optionsPanel.add(personOptionsPanel);
		optionsPanel.add(accountOptionsPanel);
		optionsPanel.add(operationOptionsPanel);
		
		//Person table add/delete/update
		personOptionsPanel.setLayout(new GridBagLayout());
		GridBagConstraints personConstr = new GridBagConstraints();
		personConstr.fill = GridBagConstraints.HORIZONTAL;
		
		personConstr.gridx = 0;
		personConstr.gridy = 0;
		personOptionsPanel.add(persIdLabel, personConstr);
		personConstr.gridx = 1;
		personConstr.gridy = 0;
		personOptionsPanel.add(persIdField, personConstr);
		
		personConstr.gridx = 0;
		personConstr.gridy = 1;
		personOptionsPanel.add(firstNameLabel, personConstr);
		personConstr.gridx = 1;
		personConstr.gridy = 1;
		personOptionsPanel.add(firstNameField, personConstr);
		
		personConstr.gridx = 0;
		personConstr.gridy = 2;
		personOptionsPanel.add(lastNameLabel, personConstr);
		personConstr.gridx = 1;
		personConstr.gridy = 2;
		personOptionsPanel.add(lastNameField, personConstr);
		
		personConstr.gridx = 0;
		personConstr.gridy = 3;
		personOptionsPanel.add(persAddrLabel, personConstr);
		personConstr.gridx = 1;
		personConstr.gridy = 3;
		personOptionsPanel.add(persAddrField, personConstr);
		
		personConstr.gridx = 0;
		personConstr.gridy = 4;
		personOptionsPanel.add(emailLabel, personConstr);
		personConstr.gridx = 1;
		personConstr.gridy = 4;
		personOptionsPanel.add(emailField, personConstr);
		
		personConstr.gridx = 0;
		personConstr.gridy = 5;
		personOptionsPanel.add(phoneLabel, personConstr);
		personConstr.gridx = 1;
		personConstr.gridy = 5;
		personOptionsPanel.add(phoneField, personConstr);
		
		personConstr.gridx = 1;
		personConstr.gridy = 6;
		personOptionsPanel.add(createPersonBtn, personConstr);
		personConstr.gridx = 1;
		personConstr.gridy = 7;
		personOptionsPanel.add(deletePersonBtn, personConstr);
		personConstr.gridx = 1;
		personConstr.gridy = 8;
		personOptionsPanel.add(updatePersonBtn, personConstr);
		
		//Account add/delete/update
		accountOptionsPanel.setLayout(new GridBagLayout());
		GridBagConstraints accountConstr = new GridBagConstraints();
		accountConstr.fill = GridBagConstraints.HORIZONTAL;
		
		accountConstr.gridx = 0;
		accountConstr.gridy = 0;
		accountOptionsPanel.add(accIdLabel, accountConstr);
		accountConstr.gridx = 1;
		accountConstr.gridy = 0;
		accountOptionsPanel.add(accIdField, accountConstr);
		
		accountConstr.gridx = 0;
		accountConstr.gridy = 1;
		accountOptionsPanel.add(PINLabel, accountConstr);
		accountConstr.gridx = 1;
		accountConstr.gridy = 1;
		accountOptionsPanel.add(PINField, accountConstr);
		
		accountConstr.gridx = 0;
		accountConstr.gridy = 2;
		accountOptionsPanel.add(accTypeLabel, accountConstr);
		accountConstr.gridx = 1;
		accountConstr.gridy = 2;
		accountOptionsPanel.add(accTypeField, accountConstr);
		
		accountConstr.gridx = 0;
		accountConstr.gridy = 3;
		accountOptionsPanel.add(balanceLabel, accountConstr);
		accountConstr.gridx = 1;
		accountConstr.gridy = 3;
		accountOptionsPanel.add(balanceField, accountConstr);
		
		accountConstr.gridx = 0;
		accountConstr.gridy = 4;
		accountOptionsPanel.add(interestLabel, accountConstr);
		accountConstr.gridx = 1;
		accountConstr.gridy = 4;
		accountOptionsPanel.add(interestField, accountConstr);
		
		accountConstr.gridx = 1;
		accountConstr.gridy = 5;
		accountOptionsPanel.add(createSavingAccountBtn, accountConstr);
		accountConstr.gridx = 1;
		accountConstr.gridy = 6;
		accountOptionsPanel.add(createSpendingAccountBtn, accountConstr);
		accountConstr.gridx = 1;
		accountConstr.gridy = 7;
		accountOptionsPanel.add(deleteAccountBtn, accountConstr);
		accountConstr.gridx = 1;
		accountConstr.gridy = 8;
		accountOptionsPanel.add(updateAccountBtn, accountConstr);
		
		//Account actions
		operationOptionsPanel.setLayout(new GridBagLayout());
		GridBagConstraints operationConstr = new GridBagConstraints();
		operationConstr.fill = GridBagConstraints.HORIZONTAL;
		
		operationConstr.gridx = 0;
		operationConstr.gridy = 0;
		operationOptionsPanel.add(amountLabel, operationConstr);
		operationConstr.gridx = 1;
		operationConstr.gridy = 0;
		operationOptionsPanel.add(amountField, operationConstr);
		
		operationConstr.gridx = 0;
		operationConstr.gridy = 1;
		operationOptionsPanel.add(depositBtn, operationConstr);
		operationConstr.gridx = 1;
		operationConstr.gridy = 1;
		operationOptionsPanel.add(withdrawBtn, operationConstr);
		operationConstr.gridx = 0;
		operationConstr.gridy = 2;
		operationOptionsPanel.add(clearBtn, operationConstr);
		operationConstr.gridx = 1;
		operationConstr.gridy = 2;
		operationOptionsPanel.add(exitBtn, operationConstr);
		
		mainFrame.setVisible(true);
	}
}
